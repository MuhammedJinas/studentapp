const app = require('express')();
const morgan =require('morgan');
const bodyParser = require('body-parser');
const { serverConfig } = require('../config');

app.use(morgan('dev'));
app.use(bodyParser.json())
app.get('',(req,res)=>{
    let response ={
        msg :' servser is running'
    }
    res.status(200).json(response);

});
app.use(require('./routes'));
app.get('*',(req,res)=>{
    let response={
        msg :'page is not found'
    }
    res.status(404).json(response);

});
app.listen(serverConfig.port, serverConfig.host,()=>{
    console.log('server is running');

});
module.exports =app;
