const { table } = require('../../orm');

/**
 * @description its used to get all course dertails
 * @param
 * @returns
 */
function getAllCourseDetails(page, limit) {
    return table('courses').select('id', 'course_name', 'course_code').orderBy('id', 'asc').forPage(page, Number.parseInt(limit)).all();
}

/**
 * @description   function which is used to get  course details
 * @param  
 * @returns 
 */
function getCourseDetails(id) {
    return table('courses').select('id', 'course_name', 'course_code', 'course_details', 'course_content').where('id', '=', id).first();

}

/**
 * @description   function which is used to create course details
 * @param {string} course_name of the course
 * @param {string} course_code of the course
 * @param {string} course_details of the course
 * @param {string} course_content of the course
 * @returns
 */
function createCourse(course_name, course_code, course_details, course_content) {
    const course = {
        course_name: course_name,
        course_code: course_code,
        course_details: course_details,
        course_content: course_content
    }
    return table('courses').insert(course);
}

/**
 * @description   function which is used to update course details
 * @param {number} id of the course to update
 * @param {string} course_name of the corse
 * @param {string} course_code of the course
 * @param {string} course_details  of the course 
 * @param {string} course_content of the course
 * @returns 
 */
function updateCourse(id, course_name, course_code, course_details, course_content) {
    const course = {
        course_name: course_name,
        course_code: course_code,
        course_details: course_details,
        course_content: course_content
    }
    return table('courses').where('id', '=', id).update(course);
}

/**
 * @description   function which is used to delete course
 * @param  { number } id course_id
 * @returns 
 */
function deleteCourse(id) {
    return table('courses').where('id', '=', id).delete()

}

module.exports = {
    getAllCourseDetails,
    getCourseDetails,
    createCourse,
    updateCourse,
    deleteCourse
};