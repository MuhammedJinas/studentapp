const { table } = require('../../orm');

/**
 * @description its used to get all Entroll dertails
 * @param
 * @returns
 */
/*function getAllEntrollDetails(page, limit) {
    return table('entroll_courses').select('id', 'students_id', 'course_id').orderBy('id', 'desc').forPage(page, Number.parseInt(limit)).all();
}

/**
 * @description   function which is used to get entroll details
 * @param  
 * @returns 
 */
function getEntrollDetails(id) {
    return table('entroll_courses').select('id','students_id','course_id').where('id', '=', id).first();

}

module.exports = {
    //getAllEntrollDetails,
    getEntrollDetails
};