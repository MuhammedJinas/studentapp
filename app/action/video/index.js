const { table } = require('../../orm');

/**
 * @description its used to get all video details
 * @param
 * @returns
 */
function getAllVideoDetails(chapter_id,  subject_id, page, limit) {
    return table('videos').select('id', 'course_id', 'subject_id', 'chapter_id', 'name', 'video_url', 'video_length').where(p => {
        q.where('subject_id', '=', subject_id);
        q.where('chapter_id', '=', chapter_id);
        q.where('course_id', '=',course_id);
    }).orderBy('id', 'asc').forPage(page, Number.parseInt(limit)).all();
}

/**
 * @description   function which is used to get  subject details
 * @param  
 * @returns 
 */
function getVideoDetails(id) {
    return table('videos').select('id', 'course_id', 'subject_id', 'chapter_id', 'name', 'video_url', 'video_length').where('id', '=', id).first();

}

/**
 * @description   function which is used to create subject details
 * @param {string} course_id of the student
 * @param {string} subject_id of the course
 * @param {string} chapter_id of the subject
 * @param {string} name  of the video
 * @param {string} video_url of the video
 * @param {string} video_length of the video
 * @returns
 */
function createVideo(course_id, subject_id, chapter_id, name, video_url, video_length) {
    const video = {
        course_id: course_id,
        subject_id: subject_id,
        chapter_id: chapter_id,
        name: name,
        video_url: video_url,
        video_length: video_length
    }
    return table('videos').insert(video);
}

/**
 * @description   function which is used to create video details
 * @param {string} course_id of the student
 * @param {string} subject_id of the course
 * @param {string} chapter_id of the subject
 * @param {string} name  of the video
 * @param {string} video_url of the video
 * @param {string} video_length of the video
 * @returns
 */

function updateVideo(id, course_id, subject_id, chapter_id, name, video_url, video_length) {
    const video = {
        course_id: course_id,
        subject_id: subject_id,
        chapter_id: chapter_id,
        name: name,
        video_url: video_url,
        video_length: video_length
    }
    return table('videos').where('id', '=', id).update(video);
}

/**
 * @description   function which is used to delete video
 * @param  { number } id subject_id
 * @returns 
 */
function deleteVideo(id) {
    return table('videos').where('id', '=', id).delete()

}

module.exports = {
    getAllVideoDetails,
    getVideoDetails,
    createVideo,
    updateVideo,
    deleteVideo
};