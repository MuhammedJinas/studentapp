const { table } = require('../../orm');

/**
 * @description its used to get all chapter details
 * @param
 * @returns
 */
function getAllChapterDetails(course_id, subject_id, page, limit) {
    return table('chapters').select('id', 'course_id', 'subject_id', 'discription').where(q => {
        q.where('subject_id', '=', subject_id);
        q.where('course_id', '=', course_id);
    }).orderBy('id', 'asc').forPage(page, Number.parseInt(limit)).all();
}

/**
 * @description   function which is used to get chapter details
 * @param  
 * @returns 
 */
function getChapterDetails(id) {
    return table('chapters').select('id', 'course_id', 'subject_id', 'discription').where('id', '=', id).first();

}

/**
 * @description   function which is used to create chapter details
 * @param {string} course_id of the subject
 * @param {string} subject_id of the chapter
 * @param {string} id of the chapter
 * @param {string} discription of the chapter
 * @returns
 */
function createChapter(course_id, subject_id, discription) {
    const chapter = {
        course_id: course_id,
        subject_id: subject_id,
        discription: discription

    }
    return table('chapters').insert(chapter);
}

/**
 * @description   function which is used to update chapter details
 * @param {number} id of the chapter to update
 * @param {string} course_id of the chapter
 * @param {string} subject_id of the chapter
 * @param {string} discription of the chapter
 * @returns 
 */
function updateChapter(id, course_id, subject_id, discription, ) {
    const chapter = {
        course_id: course_id,
        subject_id: subject_id,
        discription: discription
    }
    return table('chapters').where('id', '=', id).update(chapter);
}

/**
 * @description   function which is used to delete chapter
 * @param  { number } id chapter_id
 * @returns 
 */
function deleteChapter(id) {
    return table('chapters').where('id', '=', id).delete()

}

module.exports = {
    getAllChapterDetails,
    getChapterDetails,
    createChapter,
    updateChapter,
    deleteChapter
};