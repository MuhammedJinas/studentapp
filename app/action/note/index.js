const { table } = require('../../orm');

/**
 * @description its used to get all note details
 * @param
 * @returns
 */
function getAllNoteDetails(course_id,subject_id,chapter_id,video_id,page, limit) {
    return table('notes').select('id', 'course_id', 'subject_id', 'chapter_id', 'video_id', 'name', 'discription').where(q=> {
        q.where('subject_id', '=', subject_id);
        q.where('chapter_id', '=', chapter_id);
        q.where('video_id', '=', video_id);
        q.where('course_id', '=', course_id);
    }).orderBy('id', 'asc').forPage(page, Number.parseInt(limit)).all();
}

/**
 * @description   function which is used to get  note details
 * @param  
 * @returns 
 */
function getNoteDetails(id) {
    return table('notes').select('id', 'course_id', 'subject_id', 'chapter_id', 'video_id', 'name', 'discription').where('id', '=', id).first();

}

/**
 * @description   function which is used to create subject details
 * @param {string} course_id of the student
 * @param {string} subject_id of the course
 * @param {string} chapter_id of the subject
 * @param {string} video_id of the chapter
 * @param {string} name  of the note
 * @param {string} discription of the note
 * @returns
 */
function createNote(course_id, subject_id, chapter_id, video_id, name, discription) {
    const note = {
        course_id: course_id,
        subject_id: subject_id,
        chapter_id: chapter_id,
        video_id: video_id,
        name: name,
        discription: discription
    }
    return table('notes').insert(note);
}

/**
 * @description   function which is used to create subject details
 * @param {string} course_id of the student
 * @param {string} subject_id of the course
 * @param {string} chapter_id of the subject
 * @param {string} video_id of the chapter
 * @param {string} name  of the note
 * @param {string} discription of the note
 * @returns
 */

function updateNote(id, course_id, subject_id, chapter_id, video_id, name, discription) {
    const note = {
        course_id: course_id,
        subject_id: subject_id,
        chapter_id: chapter_id,
        video_id: video_id,
        name: name,
        discription: discription
    }
    return table('notes').where('id', '=', id).update(note);
}

/**
 * @description   function which is used to delete note
 * @param  { number } id subject_id
 * @returns 
 */
function deleteNote(id) {
    return table('notes').where('id', '=', id).delete()

}

module.exports = {
    getAllNoteDetails,
    getNoteDetails,
    createNote,
    updateNote,
    deleteNote
};