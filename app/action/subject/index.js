const { table } = require('../../orm');

/**
 * @description its used to get all subject details
 * @param
 * @returns
 */
function getAllSubjectDetails(course_id, page, limit) {
    return table('subject').select('id', 'course_id', 'name', 'code', 'details').where('course_id', '=', course_id).orderBy('id', 'asc').forPage(page, Number.parseInt(limit)).all();
}

/**
 * @description   function which is used to get  subject details
 * @param  
 * @returns 
 */
function getSubjectDetails(id) {
    return table('subject').select('id', 'course_id', 'name', 'code', 'details').where('id', '=', id).first();

}

/**
 * @description   function which is used to create subject details
 * @param {string} name of the subject
 * @param {string} code of the subject
 * @param {string} details of the subject
 * @param {string} course_id of the subject
 * @returns
 */
function createSubject(course_id, name, code, details) {
    const subject = {
        course_id: course_id,
        name: name,
        code: code,
        details: details
    }
    return table('subject').insert(subject);
}

/**
 * @description   function which is used to update course details
 * @param {number} id of the subject to update
 * @param {string} name of the subject
 * @param {string} code of the subject
 * @param {string} course_id of the subject
 * @returns 
 */
function updateSubject(id, course_id, name, code, details) {
    const subject = {
        course_id: course_id,
        name: name,
        code: code,
        details: details
    }
    return table('subject').where('id', '=', id).update(subject);
}

/**
 * @description   function which is used to delete subject
 * @param  { number } id subject_id
 * @returns 
 */
function deleteSubject(id) {
    return table('subject').where('id', '=', id).delete()

}

module.exports = {
    getAllSubjectDetails,
    getSubjectDetails,
    createSubject,
    updateSubject,
    deleteSubject
};