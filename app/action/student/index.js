const { table } = require('../../orm');

/**
 * @description its used to get all students dertails
 * @param
 * @returns
 */
function getAllStudentDetails(page, limit) {
    return table('students').select('id', 'name', 'email').orderBy('id', 'desc').forPage(page, Number.parseInt(limit)).all();
}

/**
 * @description   function which is used to get students details
 * @param  
 * @returns 
 */
function getStudentDetails(id) {
    return table('students').select('id','name','email','phone_number', 'adress','parent_name').where('id', '=', id).first();

}

/**
 * @description   function which is used to create student details
 * @param {string} name of the student
 * @param {string} email of the student
 * @param {string} phone_number of the student
 * @param {string} adress of the student
 * @param { string} parent_name of the student
 * @returns 
 */
function createStudent(name, email, phone_number, adress, parent_name) {
    const student = {
        name: name,
        email: email,
        phone_number: phone_number,
        adress: adress,
        parent_name: parent_name
    }
    return table('students').insert(student);
}

/**
 * @description   function which is used to update students details
 * @param {number} id of the student to update
 * @param {string} name of the student
 * @param {string} email of the student
 * @param {string} phone_number of the student
 * @param {string} adress ofthe student
 * @param {string} parent_name of the student
 * @returns 
 */
function updateStudent(id, name, email, phone_number, adress, parent_name) {
    const student = {
        name: name,
        email: email,
        phone_number: phone_number,
        adress: adress,
        parent_name: parent_name
    }
    return table('students').where('id', '=', id).update(student);
}

/**
 * @description   function which is used to delete student details
 * @param  { number } id todos id
 * @returns 
 */
function deleteStudent(id) {
    return table('students').where('id', '=', id).delete()

}

module.exports = {
    course:require('./course'),
    getAllStudentDetails,
    getStudentDetails,
    createStudent,
    updateStudent,
    deleteStudent
};