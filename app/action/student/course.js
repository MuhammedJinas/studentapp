const { table } = require('../../orm');

/**
 * @description   function which is used to get student course details
 * @param  
 * @returns 
 */
  function getStudentCourseDetails(id) {
    return  table('entroll_courses').eagerLoad('courses').where('students_id','=', id).all();

}


module.exports = {
    getStudentCourseDetails
};