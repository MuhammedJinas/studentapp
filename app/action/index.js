module.exports = {
    course: require('./course'),
    student: require('./student'),
    subject: require('./subject'),
    chapter: require('./chapter'),
    video: require('./video'),
    entroll: require('./entroll'),
    note: require('./note')
}