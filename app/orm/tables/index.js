const courses = require('./courses');
const students = require('./students');
const subject = require('./subject');
const chapters = require('./chapters');
const videos = require('./videos');
const notes = require('./notes');
const entroll_courses = require('./entroll_courses');
//const student_courses = require('./student_courses');
function loadTables(orm) {
    courses(orm);
    students(orm);
    subject(orm);
    chapters(orm);
    videos(orm);
    notes(orm);
    entroll_courses(orm);
   // student_courses(orm);
}
module.exports = loadTables