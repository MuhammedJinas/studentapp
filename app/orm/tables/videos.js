module.exports = (orm) => {
    orm.defineTable({
        name: 'videos',
        props: {
            timestamps: true
        }
    });
};