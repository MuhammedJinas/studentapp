module.exports = (orm) => {
    orm.defineTable({
        name: 'chapters',
        props: {
            timestamps: true
        }
    });
};