module.exports = (orm) => {
    orm.defineTable({
        name: 'students',
        props: {
            timestamps: true
        }
    });
};