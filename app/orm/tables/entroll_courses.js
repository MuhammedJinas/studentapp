module.exports = (orm) => {
    orm.defineTable({
        name: 'entroll_courses',
        props: {
            timestamps: true
        },
        relations: {
            courses() {
                return this.hasMany('courses', 'id','course_id');
            }
        }
    });
};

