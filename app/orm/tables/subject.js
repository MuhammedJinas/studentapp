module.exports = (orm) => {
    orm.defineTable({
        name: 'subject',
        props: {
            timestamps: true
        }
    });
};