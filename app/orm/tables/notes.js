module.exports = (orm) => {
    orm.defineTable({
        name: 'notes',
        props: {
            timestamps: true
        }
    });
};