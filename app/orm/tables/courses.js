module.exports = (orm) => {
    orm.defineTable({
        name: 'courses',
        props: {
            timestamps: true
        }
    });
};