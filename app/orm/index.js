
const tabel = require('tabel');
const config = require('../../config');
const loadTables = require('./tables');

const orm = new tabel(config);

loadTables(orm);

module.exports = orm.exports;