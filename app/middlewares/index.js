function authUser(req, res, next) {
    if (req.headers.auth) {
        if (req.headers.auth == 'samplesample') {
            next();
        } else {
            res.status(401).json({
                msg: 'unauth'
            })
        }
    } else {
        res.status(401).json({
            msg: 'auth is required'
        })
    }
}

function UserNameAndPassword(req, res, next) {
    if (req.body.username && req.body.password) {
        next();
    } else {
        res.status(404).json({
            msg: 'Password and username required'
        })
    }
}

function Varification(req, res, next) {
    if (req.body.name == 'jinas') {
        next();
    }
    else {
        res.status(201).json({
            msg: 'name is incorrect'
        })
    }
}

function DOB(req, res, next) {
    if (req.body.dob) {
        next();
    } else {
        res.status(201).json({
            msg: 'dob is incorrect'
        })
    }
}
function StatusField(req, res, next) {
    if (req.body.statusfield >= 1 && req.body.statusfield <= 3) {
        if (typeof (req.body.statusfield) == "number") {
            next();
        } else {
            res.status(201).json({
                msg:'status field is not a number'

            })
        }
    }
    else {
        res.status(201).json({
            msg: 'not valid'
        })
    }


}

module.exports = {
    authUser,
    UserNameAndPassword,
    Varification,
    DOB,
    StatusField
}