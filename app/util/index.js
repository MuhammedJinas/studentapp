const { validationResult } = require('express-validator/check');

function validator(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return true;
    }
    return false;
}

module.exports = {
    validator
}