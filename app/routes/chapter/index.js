const app = require('express')();
const { check } = require('express-validator/check');
const { validator } = require('../../util')

const { getAllChapterDetails,
    getChapterDetails,
    createChapter,
    updateChapter,
    deleteChapter
} = require('../../action').chapter;

/**
 * Route       : `chapter`
 * Method      : `GET`
 * Params      : `limit? , page? :URL`
 * Response    : `[ { id: string:course_id,string:subject_id,string,chapter_discription} ]` :JSON Array
 * Discribtion : it is used to get the chapter details
 */
app.get('/:course_id/subject/:subject_id/chapter', (req, res, next) => {
    if (req.query.limit) {
        next();
    } else {
        res.status(403).json({
            msg: 'limit is required'
        })
    }
}, (req, res) => { // ES6 
    let course_id = req.params.course_id;
    let subject_id = req.params.subject_id;
    let page = req.query.page || 1;
    let limit = req.query.limit || 10;
    console.log(req.body.name);

    getAllChapterDetails(course_id, subject_id, page, limit).then((response) => {
        res.status(200).json({
            data: response
        });

    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });

    });
});

/**
 * Route : chapter/{chapter_id}
 * Method : GET
 * params :{}
 * description : it is used to get the chapter_id details
 */
app.get('/:course_id/subject/:subject_id/chapter/:chapter_id', (req, res) => {
    let id = req.params.chapter_id;
    getChapterDetails(id).then((response) => {
        res.status(200).json({
            data: response
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route : chapter/{chapter_id}
 * Method : GET
 * params :{}
 * description : it is used to create  the chapter id details
 */
app.patch('/:course_id/subject/:subject_id/chapter/:chapter_id', [
    check('course_id').isInt().exists(),
    check('subject_id').isInt().exists(),
    check('discription').isString().exists().withMessage('its should be a string')
], (req, res) => {
    if (validator(req, res)) return
    let id = req.params.chapter_id;
    updateChapter(id, req.body.course_id, req.body.subject_id, req.body.discription).then((response) => {
        res.status(200).json({
            msg: 'updated'
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});


/**
 * Route : `chapter/{chapter_id}`
 * Method : DELETE
 * params :{}
 * description : it is used to delete subject details
 */
app.delete('/:course_id/subject/:subject_id/chapter/:chapter_id', (req, res) => {
    let id = req.params.chapter_id;
    deleteChapter(id).then((response) => {
        res.status(200).json({
            msg: 'deleted'
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route       : chapter
 * Method      : POST
 * params      :{}
 * describtion : it is used to create the chapter  details
 */
app.post('/:course_id/subject/:subject_id/chapter', [
    check('course_id').isInt().exists(),
    check('subject_id').isInt().exists(),
    check('discription').isString().exists()
], (req, res) => {
    if (validator(req, res)) return;
    createChapter(req.body.course_id, req.body.subject_id, req.body.discription).then(response => {
        res.status(200).json({
            msg: response
        });

    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});
module.exports = app;