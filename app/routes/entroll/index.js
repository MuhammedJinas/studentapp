const app = require('express')();
const { check } = require('express-validator/check');
const { validator } = require('../../util')

const { //getAllEntrollDetails,
    getEntrollDetails,
    //createEntroll,
    //updateEntroll,
    //deleteEntroll
} = require('../../action').entroll;

/**
 * Route       : `entroll`
 * Method      : `GET`
 * Params      : `limit? , page? :URL`
 * Response    : `[ { id: number, name: string, email: string } ]` :JSON Array
 * Discribtion : it is used to get the Entroll  details
 */
app.get('', (req, res, next) => {
    if (req.query.limit) {
        next();
    } else {
        res.status(403).json({
            msg: 'limit is required'
        })
    }
}, (req, res) => { // ES6 
    let page = req.query.page || 1;
    let limit = req.query.limit || 10;
    console.log(req.body.name);

   
});

/**
 * Route : Entroll/{Entroll_id}
 * Method : GET
 * params :{}
 * description : it is used to get the Entroll_id details
 */
app.get('/:entroll_id', (req, res) => {
    let id = req.params.entroll_id;
    getEntrollDetails(id).then((response) => {
        res.status(200).json({
            data: response
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route : Entroll/{Entroll_id}
 * Method : GET
 * params :{}
 * description : it is used to update the Entroll_id details
 */
/*app.patch('/:entroll_id', [
    check('students_id').isInt().exists(),
    check('course_id').isInt()

], (req, res) => {
    if (validator(req, res)) return
    let id = req.params.entroll_id;
    updateEntroll(id, req.body.students_id, req.body.course_id).then((response) => {
        res.status(200).json({
            msg: 'updated'
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});


/**
 * Route : `Entroll/{entroll_id}`
 * Method : DELETE
 * params :{}
 * description : it is used to delete Entroll details
 */
/*app.delete('/:entroll_id', (req, res) => {
    let id = req.params.entroll_id;
    deleteEntroll(id).then((response) => {
        res.status(200).json({
            msg: 'deleted'
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route       : students
 * Method      : POST
 * params      :{}
 * describtion : it is used to create the students details
 */
/*app.post('', [
    check('students_id').isInt().exists(),
    check('course_id').isInt()
   
], (req, res) => {
    if (validator(req, res)) return;
    createEntroll(req.body.students_id, req.body.course_id).then(response => {
        res.status(200).json({
            msg: response
        });

    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});*/
module.exports = app;