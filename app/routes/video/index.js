const app = require('express')();
const { check } = require('express-validator/check');
const { validator } = require('../../util')

const { getAllVideoDetails,
    getVideoDetails,
    createVideo,
    updateVideo,
    deleteVideo
} = require('../../action').video;

/**
 * Route       : `video`
 * Method      : `GET`
 * Params      : `limit? , page? :URL`
 * Response    : `[ { id: number, name: string, subject_code:string,subject_details} ]` :JSON Array
 * Discribtion : it is used to get the video  details
 */
app.get('/:course_id/subject/:subject_id/chapter/:chapter_id/video', (req, res, next) => {
    if (req.query.limit) {
        next();
    } else {
        res.status(403).json({
            msg: 'limit is required'
        })
    }
}, (req, res) => { // ES6 
    let course_id = req.params.course_id;
    let subject_id = req.params.subject_id;
    let chapter_id =req.params.chapter_id;
    let page = req.query.page || 1;
    let limit = req.query.limit || 10;
    console.log(req.body.name);

    getAllVideoDetails(course_id, subject_id,chapter_id,page, limit).then((response) => {
        res.status(200).json({
            data: response
        });

    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });

    });
});

/**
 * Route : video/{video_id}
 * Method : GET
 * params :{}
 * description : it is used to get the video_id details
 */
app.get('/:course_id/subject/:subject_id/chapter/:chapter_id/video/:video_id', (req, res) => {
    let id = req.params.video_id;
    getVideoDetails(id).then((response) => {
        res.status(200).json({
            data: response
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route : video/{video_id}
 * Method : GET
 * params :{}
 * description : it is used to u the video id details
 */
app.patch('/:course_id/subject/:subject_id/chapter/:chapter_id/video/:video_id', [
    check('course_id').isInt().exists(),
    check('subject_id').isInt().exists(),
    check('chapter_id').isInt().exists(),
    check('name').isString().exists().withMessage(' it shuold be a string'),
    check('video_url').isString().exists().withMessage('its should be a string'),
    check('video_length').exists()

], (req, res) => {
    if (validator(req, res)) return
    let id = req.params.video_id;
    updateVideo(id, req.body.course_id, req.body.subject_id,req.body,chapter_id, req.body.name, req.body.video_url, req.body.video_length).then((response) => {
        res.status(200).json({
            msg: 'updated'
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});


/**
 * Route : `video/{video_id}`
 * Method : DELETE
 * params :{}
 * description : it is used to delete video details
 */
app.delete('/:course_id/subject/:subject_id/chapter/:chapter_id/video/:video_id', (req, res) => {
    let id = req.params.video_id;
    deleteVideo(id).then((response) => {
        res.status(200).json({
            msg: 'deleted'
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route       : video
 * Method      : POST
 * params      :{}
 * describtion : it is used to create the subject details
 */
app.post('/:course_id/subject/:subject_id/chapter/:chapter_id/video', [
    check('course_id').isInt().exists(),
    check('subject_id').isInt().exists(),
    check('chapter_id').isInt().exists(),
    check('name').isString().exists().withMessage(' it shuold be a string'),
    check('video_url').isString().exists().withMessage('its should be a string'),
    check('video_length').exists()

  

], (req, res) => {
    if (validator(req, res)) return;
    createVideo(req.body.course_id, req.body.subject_id, req.body.chapter_id,req.body.name, req.body.video_url, req.body.video_length).then(response => {
        res.status(200).json({
            msg: response
        });

    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});
module.exports = app;   