const app = require('express')();
const { getStudentCourseDetails }= require('../../action/student/course');


app.get('', (req, res, next) => {
    if (req.query.limit) {
        next();
    } else {
        res.status(403).json({
            msg: 'limit is required'
        })
    }
}, (req, res) => { // ES6 
    let page = req.query.page || 1;
    let limit = req.query.limit || 10;
    console.log(req.body.name);

    
});

/**
 * Route : student/{student_id}
 * Method : GET
 * params :{}
 * description : it is used to get the student_id details
 */
app.get('/:students_id/course', (req, res) => {
    let id = req.params.students_id;
    getStudentCourseDetails(id).then((response) => {
        res.status(200).json({
            data: response
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});






module.exports = app;