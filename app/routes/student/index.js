const app = require('express')();
const { check } = require('express-validator/check');
const { validator } = require('../../util');

const { getAllStudentDetails,
    getStudentDetails,
    createStudent,
    updateStudent,
    deleteStudent
} = require('../../action').student;

/** ? 
 * Route       : `student`
 * Method      : `GET`
 * Params      : `limit? , page? :URL`
 * Response    : `[ { id: number, name: string, email: string } ]` :JSON Array
 * Discribtion : it is used to get the students  details
 */
app.get('', (req, res, next) => {
    if (req.query.limit) {
        next();
    } else {
        res.status(403).json({
            msg: 'limit is required'
        })
    }
}, (req, res) => { // ES6 
    let page = req.query.page || 1;
    let limit = req.query.limit || 10;
    console.log(req.body.name);


    getAllStudentDetails(page, limit).then((response) => {
        res.status(200).json({
            data: response
        });

    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });

    });
});

/**
 * Route : student/{student_id}
 * Method : GET
 * params :{}
 * description : it is used to get the student_id details
 */
app.get('/:students_id', (req, res) => {
    let id = req.params.students_id;
    getStudentDetails(id).then((response) => {
        res.status(200).json({
            data: response
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route : students/{students_id}
 * Method : GET
 * params :{}
 * description : it is used to u[date the students_id details
 */
app.patch('/:students_id', [
    check('name').isString().exists().withMessage(' it shuold be a string'),
    check('email').isEmail().exists().withMessage('its should be a Email format'),
    check('phone_number').isString().isLength({ min: 10, max: 10 }),
    check('adress').isString().withMessage('it should be a string'),
    check('parent_name').isString()


], (req, res) => {
    if (validator(req, res)) return
    let id = req.params.students_id;
    updateStudent(id, req.body.name, req.body.email, req.body.phone_number, req.body.adress, req.body.parent_name).then((response) => {
        res.status(200).json({
            msg: 'updated'
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});


/**
 * Route : `students/{students_id}`
 * Method : DELETE
 * params :{}
 * description : it is used to delete students details
 */
app.delete('/:students_id', (req, res) => {
    let id = req.params.student_id;
    deleteStudent(id).then((response) => {
        res.status(200).json({
            msg: 'deleted'
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route       : students
 * Method      : POST
 * params      :{}
 * describtion : it is used to create the students details
 */
app.post('/:students_id', [
    check('name').isString().exists().withMessage('name should be a string and its required'),
    check('phone_number').isString().isLength({ min: 10, max: 10 }),
    check('email').isEmail().withMessage('Please provide a valid email').exists().withMessage('Please provide an email'),
    check('adress').isString().exists().withMessage('should be a string'),
    check('parent_name').isString().exists().withMessage('parent_name should be string')
], (req, res) => {
    if (validator(req, res)) return;
    createStudent(req.body.name, req.body.email, req.body.phone_number, req.body.adress, req.body.parent_name).then(response => {
        res.status(200).json({
            msg: response
        });

    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});
module.exports = app;
