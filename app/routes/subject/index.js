const app = require('express')();
const { check } = require('express-validator/check');
const { validator } = require('../../util')

const { getAllSubjectDetails,
    getSubjectDetails,
    createSubject,
    updateSubject,
    deleteSubject
} = require('../../action').subject;

/**
 * Route       : `subject`
 * Method      : `GET`
 * Params      : `limit? , page? :URL`
 * Response    : `[ { id: number, name: string, subject_code:string,subject_details} ]` :JSON Array
 * Discribtion : it is used to get the subject  details
 */
app.get('/:course_id/subject', (req, res, next) => {
    if (req.query.limit) {
        next();
    } else {
        res.status(403).json({
            msg: 'limit is required'
        })
    }
}, (req, res) => { // ES6 
    let course_id = req.params.course_id;
    let page = req.query.page || 1;
    let limit = req.query.limit || 10;
    console.log(req.body.name);

    getAllSubjectDetails(course_id,page, limit).then((response) => {
        res.status(200).json({
            data: response
        });

    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });

    });
});

/**
 * Route : subject/{subject_id}
 * Method : GET
 * params :{}
 * description : it is used to get the student_id details
 */
app.get('/:course_id/subject/:subject_id', (req, res) => {
    let id = req.params.subject_id;
    getSubjectDetails(id).then((response) => {
        res.status(200).json({
            data: response
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route : subjects/{subject_id}
 * Method : GET
 * params :{}
 * description : it is used to u the subject id details
 */
app.patch('/:course_id/subject/:subject_id', [
    check('course_id').isInt().exists(),
    check('name').isString().exists().withMessage(' it shuold be a string'),
    check('code').isString().exists().withMessage('its should be a string'),
    check('details').isString().exists()

], (req, res) => {
    if (validator(req, res)) return
    let id = req.params.subject_id;
    updateSubject(id, req.body.course_id, req.body.name, req.body.code, req.body.details).then((response) => {
        res.status(200).json({
            msg: 'updated'
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});


/**
 * Route : `subject/{subject_id}`
 * Method : DELETE
 * params :{}
 * description : it is used to delete subject details
 */
app.delete('/:course_id/subject/:subject_id', (req, res) => {
    let id = req.params.subject_id;
    deleteSubject(id).then((response) => {
        res.status(200).json({
            msg: 'deleted'
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route       : subject
 * Method      : POST
 * params      :{}
 * describtion : it is used to create the subject details
 */
app.post('/:course_id/subject', [
    check('course_id').isInt().exists(),
    check('name').isString().exists().withMessage(' it shuold be a string'),
    check('code').isString().exists().withMessage('its should be a string'),
    check('details').isString().exists()

], (req, res) => {
    if (validator(req, res)) return;
    createSubject(req.body.course_id, req.body.name, req.body.code, req.body.details).then(response => {
        res.status(200).json({
            msg: response
        });

    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});
module.exports = app;