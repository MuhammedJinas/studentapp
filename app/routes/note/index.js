const app = require('express')();
const { check } = require('express-validator/check');
const { validator } = require('../../util')

const { getAllNoteDetails,
    getNoteDetails,
    createNote,
    updateNote,
    deleteNote
} = require('../../action').note;

/**
 * Route       : `note`
 * Method      : `GET`
 * Params      : `limit? , page? :URL`
 * Response    : `[ { id: number, name: string} ]` :JSON Array
 * Discribtion : it is used to get the note  details
 */
app.get('/:course_id/subject/:subject_id/chapter/:chapter_id/video/:video_id/note', (req, res, next) => {
    if (req.query.limit) {
        next();
    } else {
        res.status(403).json({
            msg: 'limit is required'
        })
    }
}, (req, res) => { // ES6 
    let course_id = req.params.course_id;
    let subject_id = req.params.subject_id;
    let chapter_id =req.params.chapter_id;
    let video_id =req.params.video_id;
    let page = req.query.page || 1;
    let limit = req.query.limit || 10;
    console.log(req.body.name);

    getAllNoteDetails(course_id, subject_id,chapter_id,video_id,page, limit).then((response) => {
        res.status(200).json({
            data: response
        });

    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });

    });
});

/**
 * Route : note/{note_id}
 * Method : GET
 * params :{}
 * description : it is used to get the vnote_id details
 */
app.get('/:course_id/subject/:subject_id/chapter/:chapter_id/video/:video_id/note/:note_id', (req, res) => {
    let id = req.params.note_id;
    getNoteDetails(id).then((response) => {
        res.status(200).json({
            data: response
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route : note/{note_id}
 * Method : GET
 * params :{}
 * description : it is used to u the note id details
 */
app.patch('/:course_id/subject/:subject_id/chapter/:chapter_id/video/:video_id/note/:note_id', [
    check('course_id').isInt().exists(),
    check('subject_id').isInt().exists(),
    check('chapter_id').isInt().exists(),
    check('video_id').isInt().exists(),
    check('name').isString().exists().withMessage(' it shuold be a string'),
    check('discription').isString().exists().withMessage('its should be a string')

], (req, res) => {
    if (validator(req, res)) return
    let id = req.params.note_id;
    updateNote(id, req.body.course_id, req.body.subject_id, req.body.chapter_id, req.body.video_id, req.body.name, req.body.discription).then((response) => {
        res.status(200).json({
            msg: 'updated'
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});


/**
 * Route : `note/{note_id}`
 * Method : DELETE
 * params :{}
 * description : it is used to delete note details
 */
app.delete('/:course_id/subject/:subject_id/chapter/:chapter_id/video/:video_id/note/:note_id', (req, res) => {
    let id = req.params.note_id;
    deleteNote(id).then((response) => {
        res.status(200).json({
            msg: 'deleted'
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route       : note
 * Method      : POST
 * params      :{}
 * describtion : it is used to create the note details
 */
app.post('/:course_id/subject/:subject_id/chapter/:chapter_id/video/:video_id/note', [
    check('course_id').isInt().exists(),
    check('subject_id').isInt().exists(),
    check('chapter_id').isInt().exists(),
    check('video_id').isInt().exists(),
    check('name').isString().exists().withMessage(' it shuold be a string'),
    check('discription').isString().exists().withMessage('its should be a string')
], (req, res) => {
    if (validator(req, res)) return;
    createNote(req.body.course_id, req.body.subject_id, req.body.chapter_id, req.body.video_id, req.body.name, req.body.discription).then(response => {
        res.status(200).json({
            msg: response
        });

    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});
module.exports = app;