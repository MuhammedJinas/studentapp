const app = require('express')();
const { check } = require('express-validator/check');
const { validator } = require('../../util');

const { getAllCourseDetails,
    getCourseDetails,
    createCourse,
    updateCourse,
    deleteCourse
} = require('../../action').course;
/**
 * Route       : course
 * Method      : GET
 * params      :{}
 * discribtion :it is used to get course  details
 */
app.get('', (req, res) => {
    let page = req.query.page || 1;
    let limit = req.query.limit || 10;
    getAllCourseDetails(page, limit).then((response) => {
        res.status(200).json({
            data: response
        });

    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });

    });
});

/**
 * Route : course/{courses_id}
 * Method : GET
 * params :{}
 * description : it is used to get the course_id details
 */
app.get('/:course_id',(req, res) => {
    let id = req.params.course_id;
    getCourseDetails(id).then((response) => {
        res.status(200).json({
            data: response
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route       : course
 * Method      : POST
 * params      :{}
 * describtion : it is used to create the course details
 */
app.post('', [
    check('course_name').isString().exists().withMessage('name  should be required'),
    check('course_code').isString().exists().isLength({ max: 15 }),
    check('course_details').isString().exists(),
    check('course_content').isString().exists().withMessage('content should be required')
], (req, res) => {
    if (validator(req, res)) return
    createCourse(req.body.course_name, req.body.course_code, req.body.course_details, req.body.course_content).then(response => {
        res.status(200).json({
            msg: response
        });

    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});

/**
 * Route : course/{course_id}
 * Method : GET
 * params :{}
 * description : it is used to update_id details
 */
app.patch('/:course_id', [
    check('course_name').isString().exists().withMessage('name  should be required'),
    check('course_code').isString().exists().isLength({ max: 15 }),
    check('course_details').isString().exists(),
    check('course_content').isString().exists().withMessage('content should be required')
], (req, res) => {
    if (validator, (req, res)) return
    let id = req.params.course_id;
    updateCourse(id, req.body.course_name, req.body.course_code, req.body.course_details, req.body.course_content).then((response) => {
        res.status(200).json({
            msg: 'updated'
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            msg: 'server error'
        });
    });
});


/**
 * Route : course/{course_id}
 * Method : DELETE
 * params :{}
 * description : it is used to delete courses
 */
app.delete('/:course_id', (req, res) => {
    let id = req.params.course_id;
    deleteCourse(id).then((response) => {
        res.status(200).json({
            msg: 'deleted'
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            msg: 'server error'
        });
    });
});

module.exports = app;