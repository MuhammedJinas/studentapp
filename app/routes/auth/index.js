const app = require('express')();
const { authUser, UserNameAndPassword, Varification, DOB,StatusField } = require('../../middlewares');

app.post('/login', authUser, UserNameAndPassword, Varification, DOB,StatusField ,(req, res) => {
    res.status(200).json({
        msg: 'you are authenticated'
    })
})

module.exports = app;