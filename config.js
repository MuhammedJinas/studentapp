module.exports = {
    serverConfig: {
        port: 3000,
        host: 'localhost'
    },
    db: {
        client: 'postgresql',
        connection: {
            database: 'fourth_project',
            host: 'localhost',
            port: 5432,
            user: 'postgres',
            // password: 'dev'
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: 'knex_migrations'
    },
    // redis config is optional, is used for caching by tabel
    //   redis: {
    //     host: 'localhost',
    //     port: '6379',
    //     keyPrefix: 'dev.api.'
    //   }
}