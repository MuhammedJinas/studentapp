function up(knex) {
    return knex.schema.createTable('entroll_courses', (table) => {
        table.increments('id').primary().unique();
        table.integer('students_id');
        table.integer('course_id');
        table.timestamps();
    })

}

function down(knex) {
    return knex.schema.dropTable('entroll_courses')
}

module.exports = {up, down};
