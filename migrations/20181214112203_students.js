function up(knex) {
    return knex.schema.createTable('students', (table) => {
        table.increments('id').primary().unique();
        table.string('name', 50);
        table.string('email', 50);
        table.string('phone_number', 10);
        table.string('adress', 300);
        table.string('parent_name', 100);
        table.timestamps();

    });
}

function down(knex) {
    return knex.schema.dropTable('students');

}

module.exports = { up, down };
