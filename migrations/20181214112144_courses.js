function up(knex) {
    return knex.schema.createTable('courses',(table)=>{
        table.increments('id').primary().unique();
        table.string('course_name',100);
        table.string('course_code',10);
        table.string('course_details',200);
        table.string('course_content',300);
        table.timestamps();
    })

}

function down(knex) {
    return knex.schema.dropTable('courses');

}

module.exports = {up, down};