function up(knex) {
return knex.schema.createTable('chapters',(table)=>{
    table.increments('id').primary().unique();
        table.integer('course_id');
        table.integer('subject_id');
        table.string('discription', 500);
        table.timestamps();
})
}

function down(knex) {
    return knex.schema.dropTable('chapters')
}

module.exports = {up, down};
