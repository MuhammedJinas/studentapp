function up(knex) {
    return knex.schema.createTable('notes', (table) => {
        table.increments('id').primary().unique();
        table.integer('course_id');
        table.integer('subject_id');
        table.integer('chapter_id');
        table.integer('video_id');
        table.string('name', 100);
        table.string('discription', 1000);
        table.timestamps();
    })
}
function down(knex) {
    return knex.schema.dropTable('notes')
}

module.exports = { up, down };
