function up(knex) {
    return knex.schema.createTable('subject', (table) => {
        table.increments('id').primary().unique();
        table.integer('course_id');
        table.string('name', 50);
        table.string('code', 10);
        table.string('details', 200);
        table.timestamps();
    });
};

function down(knex) {
    return knex.schema.dropTable('subject')
}

module.exports = { up, down };
