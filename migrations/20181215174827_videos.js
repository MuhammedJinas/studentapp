function up(knex) {
    return knex.schema.createTable('videos', (table) => {
        table.increments('id').primary().unique();
        table.integer('course_id');
        table.integer('subject_id');
        table.integer('chapter_id');
        table.string('name', 100);
        table.string('video_url', 100);
        table.integer('video_length');
        table.timestamps();
    })

}

function down(knex) {
    return knex.schema.dropTable('videos')
}

module.exports = { up, down };
